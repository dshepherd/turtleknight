package com.TurtleKnight;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;

public class DesktopGame {
        public static void main (String[] args) {
                new LwjglApplication(new TurtleKnight(), "Turtle Knight", 480, 320, false);
        }
}
