package com.TurtleKnight;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class MainMenuScreen implements Screen {
    final TurtleKnight game;

    OrthographicCamera camera;
    
    Texture mainmenu;
    SpriteBatch batch1 = new SpriteBatch();
    public MainMenuScreen(final TurtleKnight gam) {
            game = gam;

            camera = new OrthographicCamera();
            camera.setToOrtho(false, 1024, 512); //800 480

    }
    
    @Override
    public void render(float delta) {
            Gdx.gl.glClearColor(0, 0, 0.2f, 1);
            Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);

            camera.update();
            
            batch1.setProjectionMatrix(camera.combined);

            mainmenu = new Texture("data/mainmenu3.png");
            batch1.begin();
            batch1.draw(mainmenu, 0,0);
            batch1.end();
            int height = Gdx.graphics.getHeight();
            int width = Gdx.graphics.getWidth();
            if (Gdx.input.justTouched() && (Gdx.input.getX() < width/1.19 && Gdx.input.getX() > width/1.74) && (Gdx.input.getY() < height/1.37 && Gdx.input.getY() > height/2.43)) {
                    game.setScreen(new LevelSelectScreen(game));
                    dispose();
            }
    }

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void show() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		mainmenu.dispose();
		batch1.dispose();
		game.dispose();
	}
    
    
}
