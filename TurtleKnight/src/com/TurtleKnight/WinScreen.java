package com.TurtleKnight;

import com.TurtleKnight.GameScreen.Turtle;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class WinScreen implements Screen {
    final TurtleKnight game;

    OrthographicCamera camera;
    SpriteBatch batch1 = new SpriteBatch();
    BitmapFont font = new BitmapFont();
    Texture winscreen;
    
    public WinScreen(final TurtleKnight gam) {
            game = gam;

            camera = new OrthographicCamera();
            camera.setToOrtho(false, 1024, 512); //800 480

    }
    
    @Override
    public void render(float delta) {
            Gdx.gl.glClearColor(0, 0, 0.2f, 1);
            Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);

            camera.update();
            batch1.setProjectionMatrix(camera.combined);

            winscreen = new Texture("data/background.png");
            batch1.begin();
            batch1.draw(winscreen, 0,0);
            font.setColor(Color.RED);
            font.setScale(5, 5);
            CharSequence score = Integer.toString(Turtle.score);
            font.draw(batch1, "YOU WIN!!! ", 350, 475);
            font.setColor(Color.MAGENTA);
            font.draw(batch1, "Final score for this level: " + score, 50, 275);
            font.setColor(Color.GREEN);
            font.draw(batch1, "Retry", 45, 100);
            font.draw(batch1, "Return to level select", 300, 100);
            batch1.end();
            int height = Gdx.graphics.getHeight();
            int width = Gdx.graphics.getWidth();
            //right-left edges, bottom-top edges
            if (Gdx.input.justTouched()) {
            	if ((Gdx.input.getX() < width/4.5 && Gdx.input.getX() > width/22.39) && (Gdx.input.getY() < height/1.05 && Gdx.input.getY() > height/1.24)) {
            		//restart level
            		game.setScreen(new GameScreen(game));
            		Turtle.score = 0;
                    dispose();
            	} else if ((Gdx.input.getX() < width/1.05 && Gdx.input.getX() > width/3.41) && (Gdx.input.getY() < height/1.07 && Gdx.input.getY() > height/1.24)) {
            		//return to level select
            		game.setScreen(new LevelSelectScreen(game));
            		Turtle.score = 0;
                    dispose();
            	}
            }
    }

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void show() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		batch1.dispose();
		font.dispose();
		game.dispose();
		winscreen.dispose();
	}
    
    
}

