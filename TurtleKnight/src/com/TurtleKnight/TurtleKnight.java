package com.TurtleKnight;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class TurtleKnight extends Game {

	//SpriteBatch batch1;
	//BitmapFont font;
	
	public void create() {
		//batch1 = new SpriteBatch();
		//font = new BitmapFont();
		this.setScreen(new MainMenuScreen(this));
		//dispose();
	}
	
	public void render() {
		super.render();
	}
	
	public void dispose() {
		//batch1.dispose();
		//font.dispose();
	}
	
}
