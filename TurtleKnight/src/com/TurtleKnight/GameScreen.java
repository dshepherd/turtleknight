package com.TurtleKnight;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.BitmapFontCache;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.input.GestureDetector.GestureListener;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTile;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer.Cell;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.input.*;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.TimeUtils;
import com.badlogic.gdx.utils.Pool;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.Timer.Task;

/**
 * The following comments were not made by us. They were made by the libgdx team
 * and show the people to give credit to for the demo game "Super Koalio" that
 * we based our game on.
 * 
 * Super Mario Brothers like very basic platformer, using a tile map build via
 * <a href="http://www.mapeditor.org/>Tiled</a> and a tileset and sprites by <a
 * href="http://www.vickiwenderlich.com/">Vicky Wenderlich</a></p>
 * 
 * Shows simple platformer collision detection as well as on-the-fly map
 * modifications through destructable blocks!
 * 
 * @author mzechner
 * 
 *         Apart from the above, Turtle Knight is made by Pearl Dong, Mitch
 *         Fraser, Clive Oldridge, Jonathan Scogings and Dion Shepherd
 * 
 */

public class GameScreen implements Screen {

	final TurtleKnight game;

	/**
	 * The player character, has state and state time,
	 */
	static class Turtle {
		static float WIDTH;
		static float HEIGHT;
		static float MAX_VELOCITY = 20f;
		static float SLOW_VELOCITY = 5f;
		static float REGULAR_VELOCITY = 10f;
		static float JUMP_VELOCITY = 45f;
		static float DAMPING = 0.87f;

		enum State {
			Standing, Walking, Jumping, Dying
		}

		public Turtle(int i, int j) {
			position.set(i, j);
		}

		final Vector2 position = new Vector2();
		final Vector2 velocity = new Vector2();
		State state = State.Walking;
		float stateTime = 0;
		boolean facesRight = true;
		boolean grounded = false;

		boolean goingSlow = false;
		boolean goingFast = false;
		boolean immune = false;
		int walkingPointer = -1;
		int lives = 3;
		static int score = 0;
		boolean attacking = false;
		double attackTime = 0;
		boolean dead = false;
		boolean jumping = false;
		int fastcounter = 0;
		int slowcounter = 0;
		int normcounter = 0;

	}

	static class Enemy {
		static float WIDTH;
		static float HEIGHT;
		float stateTime = 0;
		boolean goingRight = true;
		final Vector2 position = new Vector2();
		final Vector2 startPosition = new Vector2();

		public Enemy(int i, int j) {
			position.set(i, j);
			startPosition.set(i, j);
		}

		void UpdatePosition() {
			if (goingRight) {
				if (position.x > (startPosition.x + 15)) {
					ChangeDirection();
				} else {
					position.x += 0.1;
				}
			} else {
				if (position.x < (startPosition.x - 15)) {
					ChangeDirection();
				} else {
					position.x -= 0.1;
				}
			}
		}

		void ChangeDirection() {
			if (goingRight) {
				goingRight = false;
			} else {
				goingRight = true;
			}
		}

	}

	private TiledMap map;
	private OrthogonalTiledMapRenderer renderer;
	private OrthographicCamera camera;
	private Texture turtleTexture;
	private Texture enemyTexture;
	private Texture lives;
	private Animation stand;
	private Animation stand1;
	private Animation walk;
	private Animation jump;
	public Turtle turtle;
	int mapWidth;
	int mapHeight;
	float gameTime = 0;
	float pauseTime = -1;
	SpriteBatch batch;
	private Array<Enemy> enemy = new Array<Enemy>();
	private Pool<Rectangle> rectPool = new Pool<Rectangle>() {
		@Override
		protected Rectangle newObject() {
			return new Rectangle();
		}
	};
	private Array<Rectangle> tiles = new Array<Rectangle>();

	private static final float GRAVITY = -2.5f;

	public GameScreen(final TurtleKnight gam) {
		this.game = gam;
		// load the turtle frames, split them, and assign them to Animations
		turtleTexture = new Texture("data/running.png");
		lives = new Texture("data/heart.png");
		TextureRegion[] regions = TextureRegion.split(turtleTexture, 19, 26)[0];
		stand = new Animation(0, regions[0]);
		jump = new Animation(0, regions[1]);
		walk = new Animation(0.15f, regions[2], regions[3], regions[4]);
		walk.setPlayMode(Animation.LOOP_PINGPONG);

		enemyTexture = new Texture("data/dragon.png");
		TextureRegion[] region = TextureRegion.split(enemyTexture, 128, 32)[0];
		stand1 = new Animation(0, region[0]);
		// figure out the width and height of the turtle for collision
		// detection and rendering by converting a turtle frames pixel
		// size into world units (1 unit == 16 pixels)
		Turtle.WIDTH = 1 / 16f * regions[0].getRegionWidth();
		Turtle.HEIGHT = 1 / 16f * regions[0].getRegionHeight();

		Enemy.WIDTH = 1 / 16f * region[0].getRegionWidth();
		Enemy.HEIGHT = 1 / 16f * region[0].getRegionHeight();

		// load the map, set the unit scale to 1/16 (1 unit == 16 pixels)
		map = new TmxMapLoader().load("data/test1.tmx");
		mapWidth = map.getProperties().get("width", Integer.class);
		mapHeight = map.getProperties().get("height", Integer.class);
		renderer = new OrthogonalTiledMapRenderer(map, 1 / 16f);
		GestureDetector flingInput = new GestureDetector(
				new MyGestureListener());
		MyInputProcessor simpleInputProcessor = new MyInputProcessor();
		Gdx.input.setInputProcessor(simpleInputProcessor);
		// Gdx.input.setInputProcessor(gameInput);
		// Gdx.input.setInputProcessor(flingInput);
		//InputMultiplexer multiplexer = new InputMultiplexer(flingInput,
		//		simpleInputProcessor);
		//Gdx.input.setInputProcessor(multiplexer);

		// create an orthographic camera, shows us 30x20 units of the world
		camera = new OrthographicCamera();
		camera.setToOrtho(false, 30, 20);
		camera.update();

		// create the Turtle we want to move around the world
		turtle = new Turtle(20, 10);

		enemy.add(new Enemy(40, 10));
		enemy.add(new Enemy(80, 10));
		enemy.add(new Enemy(120, 10));
		enemy.add(new Enemy(160, 10));
	}

	@Override
	public void render(float delta) {
		// clear the screen
		Gdx.gl.glClearColor(0.7f, 0.7f, 1.0f, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);

		// get the delta time
		float deltaTime = 0.0163f;
		gameTime += deltaTime;
		// update the turtle (process input, collision detection, position
		// update)
		updateTurtle(deltaTime);

		// let the camera follow the turtle, x-axis only
		camera.position.x = turtle.position.x;
		camera.update();

		// set the tile map renderer view based on what the
		// camera sees and render the map
		renderer.setView(camera);
		renderer.render();

		// render the turtle
		renderTurtle(deltaTime);
	}

	private Vector2 tmp = new Vector2();

	@SuppressWarnings("static-access")
	private void updateTurtle(float deltaTime) {
		if (gameTime > pauseTime + 1) {
			if (deltaTime == 0)
				return;
			turtle.stateTime += deltaTime;

			if (turtle.goingSlow) {
				turtle.velocity.x = Turtle.SLOW_VELOCITY;
			} else if (turtle.goingFast) {
				turtle.velocity.x = Turtle.MAX_VELOCITY;
			} else {
				turtle.velocity.x = Turtle.REGULAR_VELOCITY;
			}
			// turtle.velocity.x = Turtle.REGULAR_VELOCITY;
			if (turtle.grounded) {
				turtle.state = Turtle.State.Walking;
				turtle.facesRight = true;
			}

			if (turtle.position.y < 0) {
				turtle.velocity.x = 0;
				if (turtle.position.y < -50) {
					turtle.state = Turtle.State.Dying;
				}
			}

			// grapics.getwidth gets width of display in pixels. tilepixel width
			// is 16 pixels per tile so g.gw/16 then /2 for half the screen in
			// tiles.
			if (turtle.position.x > (mapWidth - (Gdx.graphics.getWidth() / 128))) {
				game.setScreen(new WinScreen(game));
				dispose();
			}

			// check input and apply to velocity & state
			// if (Gdx.input.isKeyPressed(Keys.SPACE) || ((Gdx.input.getDeltaX()
			// > Gdx.input.getDeltaY())) && turtle.grounded)
			if (turtle.position.y > 0) {
				if (((!Gdx.input.isKeyPressed(Keys.LEFT) && !Gdx.input.isKeyPressed(Keys.A))) &&
						(!Gdx.input.isKeyPressed(Keys.RIGHT) && !Gdx.input.isKeyPressed(Keys.D)) &&
						(!Gdx.input.isTouched())) {
						turtle.goingSlow = false;
						turtle.goingFast = false;
				}

	            Gdx.input.setCatchBackKey(true);
	            if (Gdx.input.isKeyPressed(Keys.BACK)) {
	        		turtle.score = 0;
	            	game.setScreen(new LevelSelectScreen(game));
	                dispose();
	            }
				
				if ((Gdx.input.isKeyPressed(Keys.SPACE) && turtle.grounded)) {
					turtle.velocity.y += Turtle.JUMP_VELOCITY;
					turtle.state = Turtle.State.Jumping;
					turtle.grounded = false;
				}

				// if (Gdx.input.isKeyPressed(Keys.LEFT) ||
				// Gdx.input.isKeyPressed(Keys.A) || isTouched(0, 0.11f) )
				if ((Gdx.input.isKeyPressed(Keys.LEFT) || Gdx.input.isKeyPressed(Keys.A))) {
					turtle.velocity.x = Turtle.SLOW_VELOCITY;
					turtle.goingSlow = true;
					turtle.goingFast = false;
					if (turtle.grounded)
						turtle.state = Turtle.State.Walking;
					turtle.facesRight = true;
				} 
				


				// if (Gdx.input.isKeyPressed(Keys.RIGHT) ||
				// Gdx.input.isKeyPressed(Keys.D) || isTouched(0.99f, 1))
				if ((Gdx.input.isKeyPressed(Keys.RIGHT) || Gdx.input.isKeyPressed(Keys.D))) {
					turtle.goingFast = true;
					turtle.goingSlow = false;
					turtle.velocity.x = Turtle.MAX_VELOCITY;
					if (turtle.grounded) {
						turtle.state = Turtle.State.Walking;
					}
					turtle.facesRight = true;
				} 

				
				if ((Gdx.input.isKeyPressed(Keys.CONTROL_LEFT ) || Gdx.input
						.isKeyPressed(Keys.CONTROL_RIGHT ))) {
					turtle.attackTime = System.currentTimeMillis();
					turtle.attacking = true;
				}
				
			} else {
				turtle.velocity.x = 0;
			}

			// apply gravity if we are falling
			turtle.velocity.add(0, GRAVITY);

			// clamp the velocity to the maximum, x-axis only
			if (Math.abs(turtle.velocity.x) > Turtle.MAX_VELOCITY) {
				turtle.velocity.x = Math.signum(turtle.velocity.x)
						* Turtle.MAX_VELOCITY;
			}

			// clamp the velocity to 0 if it's < 1, and set the state to
			// standign
			/*
			 * if (Math.abs(turtle.velocity.x) < 1) { turtle.velocity.x = 0; if
			 * (turtle.grounded) turtle.state = Turtle.State.Standing; }
			 */

			// multiply by delta time so we know how far we go
			// in this frame
			turtle.velocity.mul(deltaTime);

			// perform collision detection & response, on each axis, separately
			// if the turtle is moving right, check the tiles to the right of
			// it's
			// right bounding box edge, otherwise check the ones to the left
			Rectangle turtleRect = rectPool.obtain();
			turtleRect.set(turtle.position.x, turtle.position.y, Turtle.WIDTH,
					Turtle.HEIGHT);
			int startX, startY, endX, endY;

			startX = endX = (int) (turtle.position.x + Turtle.WIDTH + turtle.velocity.x);
			
			if (turtle.goingFast) {
				turtle.fastcounter++;
				if (turtle.fastcounter >= 20) {
					turtle.score+=20;
					turtle.fastcounter = 0;
				}
			}
				
			if (turtle.goingSlow) {
				turtle.slowcounter++;
				if (turtle.slowcounter >= 20) {
					turtle.score+=1;
					turtle.slowcounter = 0;
				}
			}
			if (!turtle.goingFast && !turtle.goingSlow) {
				turtle.normcounter++;
				if (turtle.normcounter >= 20) {
					turtle.score+=5;
					turtle.normcounter = 0;
				}
			}
			
			
			
			startY = (int) (turtle.position.y);
			endY = (int) (turtle.position.y + Turtle.HEIGHT);
			getLayer(startX, startY, endX, endY, tiles, 1);
			turtleRect.x += turtle.velocity.x;
			for (Rectangle tile : tiles) {
				if (turtleRect.overlaps(tile)) {
					turtle.velocity.x = 0;
					break;
				}
			}
			rectPool.freeAll(tiles);
			tiles.clear();
			// *****This is the general stars collision calculation******
			getLayer(startX, startY, endX, endY, tiles, 3);
			for (Rectangle tile : tiles) {
				if (turtleRect.overlaps(tile)) {
					TiledMapTileLayer layer = (TiledMapTileLayer) map
							.getLayers().get(3);
					layer.setCell((int) tile.x, (int) tile.y, null);
					turtle.score+=150;
					System.out.println(turtle.score);
				}
			}
			rectPool.freeAll(tiles);
			tiles.clear();
			// *****This is the red star collision calculation******
			getLayer(startX, startY, endX, endY, tiles, 4);
			for (Rectangle tile : tiles) {
				if (turtleRect.overlaps(tile)) {
					TiledMapTileLayer layer = (TiledMapTileLayer) map
							.getLayers().get(4);
					layer.setCell((int) tile.x, (int) tile.y, null);
					turtle.lives++;
					System.out.println(turtle.lives);
				}
			}
			rectPool.freeAll(tiles);
			tiles.clear();
			// *****This is the imunity stars collision calculation******

			getLayer(startX, startY, endX, endY, tiles, 5);
			for (Rectangle tile : tiles) {
				if (turtleRect.overlaps(tile)) {
					TiledMapTileLayer layer = (TiledMapTileLayer) map
							.getLayers().get(5);
					layer.setCell((int) tile.x, (int) tile.y, null);
					turtle.immune = true;
				}
			}
			rectPool.freeAll(tiles);
			tiles.clear();
			// *****This is the Spikes collision calculation******
			getLayer(startX, startY, endX, endY, tiles, 2);
			for (Rectangle tile : tiles) {
				if (turtleRect.overlaps(tile)) {
					turtle.state = turtle.state.Dying;
				}
			}
			rectPool.freeAll(tiles);
			tiles.clear();
			getLayer(startX, startY, endX, endY, tiles, 7);
			for (Rectangle tile : tiles) {
				if (turtleRect.overlaps(tile)) {
					if (turtle.velocity.y < 0
							&& (turtle.state == turtle.state.Jumping)) {
						turtle.velocity.y = 1;
						turtle.state = turtle.state.Jumping;
						break;
					}
				}
			}

			turtleRect.x = turtle.position.x;
			rectPool.freeAll(tiles);
			tiles.clear();

			// if the turtle is moving upwards, check the tiles to the top of
			// it's
			// top bounding box edge, otherwise check the ones to the bottom
			if (turtle.velocity.y > 0) {
				startY = endY = (int) (turtle.position.y + Turtle.HEIGHT + turtle.velocity.y);
			} else {
				startY = endY = (int) (turtle.position.y + turtle.velocity.y);
			}
			startX = (int) (turtle.position.x);
			endX = (int) (turtle.position.x + Turtle.WIDTH);

			getLayer(startX, startY, endX, endY, tiles, 1);
			turtleRect.y += turtle.velocity.y;
			for (Rectangle tile : tiles) {
				if (turtleRect.overlaps(tile)) {
					// we actually reset the turtle y-position here
					// so it is just below/above the tile we collided with
					// this removes bouncing :)
					if (turtle.velocity.y > 0) {
						turtle.position.y = tile.y - Turtle.HEIGHT;
						// we hit a block jumping upwards, let's destroy it!
						// TiledMapTileLayer layer = (TiledMapTileLayer)
						// map.getLayers().get(1);
						// layer.setCell((int) tile.x, (int) tile.y, null);
					} else {
						turtle.position.y = tile.y + tile.height;
						// if we hit the ground, mark us as grounded so we can
						// jump
						turtle.grounded = true;
					}
					turtle.velocity.y = 0;
					break;
				}
			}
			rectPool.freeAll(tiles);
			tiles.clear();

			for (int i = 0; i < enemy.size; i++) {
				enemy.get(i).UpdatePosition();
				Rectangle enemyRect = rectPool.obtain();
				enemyRect.set(enemy.get(i).position.x, enemy.get(i).position.y,
						Enemy.WIDTH, Enemy.HEIGHT);
				if (turtleRect.overlaps(enemyRect)) {
					// TODO Attacking?
					if (turtle.attacking) {
						enemy.removeIndex(i);
						enemy.shrink();
						if (i >= enemy.size) {
							break;
						}
					} else {
						turtle.state = turtle.state.Dying;
					}
				}

				// set the enemy to be at the same height as the current tile it
				// is sitting upon.
				getLayer((int) enemy.get(i).position.x, 0, (int) (enemy.get(i).position.x + enemy.get(i).WIDTH),30, tiles, 1);
				for (Rectangle tile : tiles) {
					if ((enemy.get(i).position.x > tile.x - tile.width)
							&& (enemy.get(i).position.x < (tile.x + tile.width))) {
						enemy.get(i).position.y = tile.y + tile.height;
					}
				}
			}

			rectPool.freeAll(tiles);
			tiles.clear();
			rectPool.free(turtleRect);

			// unscale the velocity by the inverse delta time and set
			// the latest position
			turtle.position.add(turtle.velocity);
			turtle.velocity.mul(1 / deltaTime);

			// Apply damping to the velocity on the x-axis so we don't
			// walk infinitely once a key was pressed
			turtle.velocity.x *= Turtle.DAMPING;
		}
	}

	private boolean isTouched(float startX, float endX) {
		// check if any finger is touch the area between startX and endX
		// startX/endX are given between 0 (left edge of the screen) and 1
		// (right edge of the screen)
		for (int i = 0; i < 20; i++) {
			float x = Gdx.input.getX() / (float) Gdx.graphics.getWidth();
			if (Gdx.input.isTouched(i) && (x >= startX && x <= endX)) {
				return true;
			}
		}
		return false;
	}

	public class MyInputProcessor implements InputProcessor {

		private int swipePointerId;
		private long startSwipe;
		private float startX;
		private float startY;
		private float dY;
		private float dX;

		@Override
		public boolean keyDown(int keycode) {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public boolean keyUp(int keycode) {
			// TODO Auto-generated method stub
			return false;

		}

		@Override
		public boolean keyTyped(char character) {
			// TODO Auto-generated method stub
			return false;
		}

		@SuppressWarnings("static-access")
		@Override
		public boolean touchDown(int screenX, int screenY, int pointer,
				int button) {
			// TODO Auto-generated method stub
			if ((!turtle.goingFast && !turtle.goingSlow)) {
				float x1 = Gdx.input.getX() / (float) Gdx.graphics.getWidth();
				if (Gdx.input.isTouched() && (x1 >= 0.0 && x1 <= 0.1)) {
					turtle.goingSlow = true;
					turtle.goingFast = false;
					turtle.walkingPointer = pointer;
					if (turtle.grounded)
						turtle.state = Turtle.State.Walking;
					turtle.facesRight = true;
					return true;
				} else if ((Gdx.input.isTouched() && (x1 >= 0.9) && (x1 <= 1))) {
					turtle.goingFast = true;
					turtle.goingSlow = false;
					turtle.walkingPointer = pointer;
					if (turtle.grounded)
						turtle.state = Turtle.State.Walking;
					turtle.facesRight = true;
					return true;
				}
			}

			if (pointer != turtle.walkingPointer) {
				swipePointerId = pointer;
				startSwipe = System.currentTimeMillis();
				startX = screenX;
				startY = screenY;
				return true;
			}

			return false;

		}

		@SuppressWarnings("static-access")
		@Override
		public boolean touchUp(int screenX, int screenY, int pointer, int button) {
			// TODO Auto-generated method stub
			if (pointer == turtle.walkingPointer) {
				turtle.goingFast = false;
				turtle.goingSlow = false;
				turtle.walkingPointer = -1;
			}

			
			  if (pointer == swipePointerId) {
				  dX = screenX - startX; dY = screenY - startY; System.out.println(dY);
				  if (System.currentTimeMillis() - startSwipe < 2500) {
					  if (((-1)*dY) > Math.abs(dX) && (Math.abs(dY) > 50)) {
						  if (!turtle.dead && !turtle.jumping) {
							  turtle.velocity.y = Turtle.JUMP_VELOCITY;
							  turtle.state = Turtle.State.Jumping;
							  turtle.grounded = false;
							  turtle.jumping = true;
						  } 
					  }
				  }
				  
				  swipePointerId = -1;
				  return true;
			  }
			 

			return false;
		}

		@Override
		public boolean touchDragged(int screenX, int screenY, int pointer) {
			// TODO Auto-generated method stub
			if (pointer == swipePointerId) {
				dX = screenX - startX;
				dY = screenY - startY;
				// System.out.println(turtle.dead);
				// System.out.println(turtle.grounded);
				// System.out.println(dY);
				// System.out.println((System.currentTimeMillis() -
				// startSwipe));
				if (System.currentTimeMillis() - startSwipe < 2500) {
					if (((-1) * dY) > Math.abs(dX) && (Math.abs(dY) >= 1)) {
						if (!turtle.dead && turtle.grounded) {
							// System.out.println("Made it");
							turtle.velocity.y = Turtle.JUMP_VELOCITY;
							turtle.state = Turtle.State.Jumping;
							turtle.grounded = false;
							turtle.jumping = true;
						}
					} else if (((dY) > Math.abs(dX)) && (Math.abs(dY) >= 1)) {
						System.out.println("Downwards fling");
						// TODO animation/mechanics

					} else if (((dX) > Math.abs(dY)) && (Math.abs(dX) >= 1)) {
						System.out.println("Sidewards fling");
						// TODO animation/mechanics
						turtle.attackTime = System.currentTimeMillis();
						turtle.attacking = true;
					}
				}
				swipePointerId = -1;
				return true;
			}
			return false;
		}

		@Override
		public boolean mouseMoved(int screenX, int screenY) {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public boolean scrolled(int amount) {
			// TODO Auto-generated method stub
			return false;
		}

	}

	public class MyGestureListener implements GestureListener {

		@Override
		public boolean touchDown(float x, float y, int pointer, int button) {

			return false;
		}

		@Override
		public boolean tap(float x, float y, int count, int button) {
			// TODO Auto-generated method stub

			return false;
		}

		@Override
		public boolean longPress(float x, float y) {
			// TODO Auto-generated method stub
			return false;
		}

		@SuppressWarnings("static-access")
		@Override
		public boolean fling(float velocityX, float velocityY, int button) {
			// TODO Auto-generated method stub
			/*
			 * if (!turtle.dead && !turtle.jumping) { if (((-1)*velocityY) >
			 * Math.abs(velocityX)) { turtle.velocity.y += Turtle.JUMP_VELOCITY;
			 * turtle.state = Turtle.State.Jumping; turtle.grounded = false;
			 * turtle.jumping = true; //System.out.println("Made ya jump");
			 * //turtle.goingFast = false; //turtle.goingSlow = false;
			 * turtle.walkingPointer = -1; return true; } else if (velocityY >
			 * Math.abs(velocityX)) { //Downwards swipe, spinning turtle?
			 * 
			 * } }
			 */
			// turtle.jumping = false;
			return false;
		}

		@Override
		public boolean pan(float x, float y, float deltaX, float deltaY) {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public boolean zoom(float initialDistance, float distance) {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public boolean pinch(Vector2 initialPointer1, Vector2 initialPointer2,
				Vector2 pointer1, Vector2 pointer2) {
			// TODO Auto-generated method stub
			return false;
		}

	}

	private void getLayer(int startX, int startY, int endX, int endY,
			Array<Rectangle> tiles, int layerNum) {
		TiledMapTileLayer layer = (TiledMapTileLayer) map.getLayers().get(
				layerNum);
		rectPool.freeAll(tiles);
		tiles.clear();
		for (int y = startY; y <= endY; y++) {
			for (int x = startX; x <= endX; x++) {
				Cell cell = layer.getCell(x, y);
				if (cell != null) {
					Rectangle rect = rectPool.obtain();
					rect.set(x, y, 1, 1);
					tiles.add(rect);
				}
			}
		}
	}

	@SuppressWarnings("static-access")
	private void renderTurtle(float deltaTime) {
		// based on the turtle state, get the animation frame
		TextureRegion frame = null;

		switch (turtle.state) {
		case Standing:
			frame = stand.getKeyFrame(turtle.stateTime);
			break;
		case Walking:
			frame = walk.getKeyFrame(turtle.stateTime);
			break;
		case Jumping:
			turtle.jumping = true;
			frame = jump.getKeyFrame(turtle.stateTime);
			break;
		case Dying:
			pauseTime = gameTime;

			turtle.dead = true;
			turtle.state = turtle.state.Walking;

			if (!turtle.immune) {
				turtle.lives--;
			} else {
				turtle.immune = false;
			}
			System.out.println(turtle.lives);
			frame = stand.getKeyFrame(turtle.stateTime);
			turtle.velocity.x = 0;
			if (turtle.lives == 0) {
				turtle.score = 0;
				game.setScreen(new LevelSelectScreen(game));
				dispose();
			}
			turtle.velocity.y = 0;
			turtle.position.x = turtle.position.x - 10;
			turtle.position.y = 20;
			turtle.dead = false;
			break;
		}

		// draw the turtle, depending on the current velocity
		// on the x-axis, draw the turtle facing either right
		// or left
		batch = renderer.getSpriteBatch();
		batch.begin();

		if (turtle.attacking) {
			// TODO animation?
			if ((System.currentTimeMillis() - turtle.attackTime) > 350) {
				turtle.attacking = false;
				turtle.attackTime = 0;
			}
		}
//		BitmapFont font = new BitmapFont();
//		CharSequence score = Integer.toString(turtle.score);
//		font.setScale((float) 0.1);
//		BitmapFontCache scoreBoard = new BitmapFontCache(font);
//		scoreBoard.setPosition(turtle.position.x-14, 19);
//		scoreBoard.addText(score, turtle.position.x + 10, 19);
//		scoreBoard.draw(batch);
		for (int i = 0; i < turtle.lives; i++) {
			batch.draw(lives, turtle.position.x - 15 + i, 19, 1, 1);
		}
		for (int i = 0; i < enemy.size; i++) {
			TextureRegion frame1 = stand1.getKeyFrame(enemy.get(i).stateTime);
			batch.draw(frame1, enemy.get(i).position.x,
					enemy.get(i).position.y, Enemy.WIDTH, Enemy.HEIGHT);
		}
		batch.draw(frame, turtle.position.x, turtle.position.y, Turtle.WIDTH,
				Turtle.HEIGHT);

		batch.end();

	}

	private Object death() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void dispose() {
		turtleTexture.dispose();
		enemyTexture.dispose();
		game.dispose();
		//batch.dispose();
		map.dispose();
		// renderer.dispose();
		lives.dispose();
		rectPool.clear();
		tiles.clear();
		enemy.clear();
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub

	}

	@Override
	public void pause() {

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void show() {
		// TODO Auto-generated method stub

	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub

	}
}
