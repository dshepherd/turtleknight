package com.TurtleKnight;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class LevelSelectScreen implements Screen {
    final TurtleKnight game;

    OrthographicCamera camera;
    
    Texture levelselect;
    SpriteBatch batch1 = new SpriteBatch();
    BitmapFont font = new BitmapFont();
    
    public LevelSelectScreen(final TurtleKnight gam) {
            game = gam;

            camera = new OrthographicCamera();
            camera.setToOrtho(false, 1024, 512); //800 480

    }
    
    @Override
    public void render(float delta) {
            Gdx.gl.glClearColor(0, 0, 0.2f, 1);
            Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);

            camera.update();
            
            batch1.setProjectionMatrix(camera.combined);

            levelselect = new Texture("data/levelselect.png");
            batch1.begin();
            batch1.draw(levelselect, 0,0);
            font.setColor(Color.MAGENTA);
            font.setScale(4, 4);
            font.draw(batch1, "Choose a level!", 325, 115);
            batch1.end();
            int height = Gdx.graphics.getHeight();
            int width = Gdx.graphics.getWidth();
            //right-left edges, bottom-top edges
            Gdx.input.setCatchBackKey(true);
            if (Gdx.input.isKeyPressed(Keys.BACK)) {
        		game.setScreen(new MainMenuScreen(game));
                dispose();
            }
            if (Gdx.input.justTouched()) {
            	if ((Gdx.input.getX() < width/3.59 && Gdx.input.getX() > width/11.39) && (Gdx.input.getY() < height/1.475 && Gdx.input.getY() > height/6.69)) {
            		//level 1
            		game.setScreen(new GameScreen(game));
                    dispose();
            	} else if ((Gdx.input.getX() < width/1.7 && Gdx.input.getX() > width/2.525) && (Gdx.input.getY() < height/1.455 && Gdx.input.getY() > height/6.21)) {
            		//level 2 (main menu at the moment)
            		game.setScreen(new MainMenuScreen(game));
                    dispose();
            	} else if ((Gdx.input.getX() < width/1.085 && Gdx.input.getX() > width/1.37) && (Gdx.input.getY() < height/1.44 && Gdx.input.getY() > height/6)) {
            		//level 3 (end-of-level at the moment)
            		game.setScreen(new WinScreen(game));
                    dispose();
            	}
            }
            
    }

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void show() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		levelselect.dispose();
		font.dispose();
		batch1.dispose();
		game.dispose();
	}
    
    
}